/**
 * Elogic BreezeTheme - Messages Mixin
 * Hide messages after 5 seconds
 */

$.mixin('messages', {

    /**
     * Prepare Message For Html
     *
     * @param {String} message
     * @return {String}
     */
    prepareMessage: function (message) {
        var self = this;

        $('.messages', this.element).show();

        setTimeout(function () {
            self.hideMessages();
        }, 5000);

        return message.replace(/\+/g, ' ');
    },

    /**
     * Hide sections and cookie messages
     */
    hideMessages: function () {
        this.messages = $.sections.get('messages');

        $('.messages', this.element).hide();

        $.cookies.remove('mage-messages', {
            domain: ''
        });

        this.messages([]);
    }
});

/**
 * Merge cookie messages (ajax compare) with json response messages
 */
$(document).on('customer-data-reload', function (event, data) {
    $('.messages .message').show();
});
