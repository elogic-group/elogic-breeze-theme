/**
 * Elogic BreezeTheme - Collapsible Mixin
 * Add collapsible animations back
 */

$.mixin('collapsible', {
    options: {
        animate: false
    },

    /**
     * Create function
     * Add animated attribute
     *
     * @param parent
     */
    create: function (parent) {
        parent();

        if (this.options.animate) {
            this.content.attr({
                'animated': true
            });
        }
    },

    /**
     * Open dropdown
     * Add animation
     *
     * @param parent
     */
    open: function (parent) {
        parent();

        if (this.options.animate) {
            this.content.css({maxHeight: '500px'});
        }
    },

    /**
     * Close dropdown
     * Add animation
     *
     * @param parent
     */
    close: function (parent) {
        parent();

        if (this.options.animate) {
            this.content.css({maxHeight: '0'});
        }
    },
});
