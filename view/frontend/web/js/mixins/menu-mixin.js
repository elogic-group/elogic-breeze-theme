/**
 * Elogic BreezeTheme - Menu Mixin
 * Adds back button, changes functionality for parent items
 */

$.mixin('menu', {

    /**
     * Enable mobile mode
     */
    toggleMobileMode: function () {
        var self = this;

        $('li.parent', this.element)
            .off('mouseenter.menu mouseleave.menu')
            .on('click.menu', function () {
                var dropdown = $(this).children(self.options.dropdown);

                if (!dropdown.length || dropdown.closest('li').hasClass('parent')) {
                    return;
                }

                self.open(dropdown);

                return false;
            });

        $('.button-back').each(function () {
            var parentCategory = $(this).closest('.parent'),
                parentCategoryName = parentCategory.find('[data-menu]').text();

            $(this).find('.parent-category').text(parentCategoryName);

            $(this).on('click', function () {
                var parentDropdown = $(this).parent(self.options.dropdown);

                self.close(parentDropdown);
            })
        });

        this._trigger('afterToggleMobileMode');
    },
});
