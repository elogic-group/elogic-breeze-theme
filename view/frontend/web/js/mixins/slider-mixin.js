/**
 * Elogic BreezeTheme - Slider Mixin
 * Add progress bar, slides counter
 */

$.mixin('pagebuilderSlider', {
    options: {
        progressBar: false,
        slidesCount: false,
        newTemplates: {
            slidesCount: [
                '<div class="slick-info">',
                    '<span class="current-slide"><%- currentSlide %></span>',
                    '<span class="dash">/</span>',
                    '<span class="slides-count"><%- slidesCount %></span>',
                '</div>'
            ].join(''),

            progressBar: [
                '<div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>'
            ].join('')
        }
    },

    /**
     * Add slides counter and progress bar
     *
     * @param parent
     */
    prepareMarkup: function (parent) {
        parent();

        if (this.options.slidesCount) {
            var counterTmpl = _.template(this.options.newTemplates.slidesCount);

            this.element.append(counterTmpl({
                currentSlide: 1,
                slidesCount: this.slides.length
            }));
        }

        if (this.options.progressBar) {
            var progressBar = '.progress-bar',
                calc = 100 / this.slides.length,
                progressBarTmpl = _.template(this.options.newTemplates.progressBar);

            this.element.append(progressBarTmpl());

            $(progressBar).css('background-size', calc + '% 100%').attr('aria-valuenow', calc);
        }
    },

    /**
     * Update slides counter and progress bar on current slide update
     *
     * @param parent
     */
    updateCurrentPage: function (parent) {
        parent();

        if (this.options.slidesCount) {
            var currentSlide = this.element.find('.current-slide');

            currentSlide.text(this.page + 1);
        }

        if (this.options.progressBar) {
            var progressBar = '.progress-bar',
                calc = ((this.page + 1) / this.pages.length) * 100;

            $(progressBar).css('background-size', calc + '% 100%').attr('aria-valuenow', calc);
        }
    },

    /**
     * Make progress bar full width if pages.length < 2
     *
     * @param parent
     */
    buildPagination: function (parent) {
        parent();

        var progressBar = '.progress-bar',
            calc = ((this.page + 1) / this.pages.length) * 100;

        if (this.pages.length < 2) {
            $(progressBar).css('background-size', '100% 100%').attr('aria-valuenow', '100%');
        } else {
            $(progressBar).css('background-size', calc + '% 100%').attr('aria-valuenow', calc);
        }
    }
});
