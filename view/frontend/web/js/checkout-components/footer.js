/**
 * Elogic Default - Footer accordion
 * Init on desktop, Destroy on mobile
 */

define([
    'jquery',
    'collapsible',
    'matchMedia'
], function ($) {
    'use strict';

    var container = $('.footer-column'),
        accordionOptions = {
            header : '.footer-column__title',
            content: '.footer-column__content',
            animate: true
        };

    container.each(function (index, elem) {
        var self = $(elem),
            accordion = self.collapsible(accordionOptions);

        mediaCheck({
            media: '(max-width: 767px)',
            entry: function () {
                accordion.collapsible('deactivate');
                accordion.collapsible('option', 'collapsible', true);
            },
            exit: function () {
                accordion.collapsible('activate');
                accordion.collapsible('option', 'collapsible', false);
            }
        });
    });
});
