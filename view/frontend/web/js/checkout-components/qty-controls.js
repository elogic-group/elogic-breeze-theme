/**
 * Elogic BreezeTheme - Qty Controls
 * Qty increment/decrement controls
 */

define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('elogic.qtyControls', {

        /**
         * Create function
         *
         * @private
         */
        _create: function() {
            this._initControls();
        },

        /**
         * Init controls
         *
         * @private
         */
        _initControls: function() {
            var self = this,
                el = $(self.element),
                updateBtn = $('.update-cart-item'),
                isMiniCart = el.closest('.minicart-items').length;

            el.find('.qty-control').on('click', function(e) {
                e.preventDefault();

                var iterator = $(this),
                    qtyInput = el.find('input.qty'),
                    currentQty = qtyInput.val(),
                    iteratorType = iterator.data('type');

                if (iteratorType === "decrement" && currentQty > 1) {
                    qtyInput.val(parseInt(currentQty) - 1);

                    if (isMiniCart) {
                        updateBtn.trigger('click');
                    }
                }

                if (iteratorType === "increment") {
                    qtyInput.val(parseInt(currentQty) + 1);

                    if (isMiniCart) {
                        updateBtn.show();
                    }
                }
            });
        }
    });

    return $.elogic.qtyControls;
});
