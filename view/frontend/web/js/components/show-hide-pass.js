/**
 * Elogic BreezeTheme - Show/Hide pass widget
 * Show/hide password functionality
 */

(function () {
    'use strict';

    $.widget('showHidePass', {
        component: 'showHidePass',

        /**
         * Create function
         *
         * @private
         */
        create: function () {
            var el = this.element,
                passInput = $(el).find('input');

            var showPassIcon = '<span class="show-pass __show">\n' +
                '<span class="icon icon-show-pass">show\n' +
                '</span>\n' +
                '<span class="icon icon-hide-pass">hide\n' +
                '</span>\n' +
                '</span>';

            $(el).append(showPassIcon);

            $(el).find('.show-pass').on('click', function () {
                if (passInput.attr('type') === 'password') {
                    $(this).removeClass('__show').addClass('__hide');
                    passInput.attr('type', 'text');
                } else {
                    $(this).removeClass('__hide').addClass('__show');
                    passInput.attr('type', 'password');
                }
            });
        }
    })
})();
