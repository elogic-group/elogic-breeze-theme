/**
 * Elogic BreezeTheme - Qty Controls
 * Qty increment/decrement controls
 */

(function () {
    'use strict';

    $.widget('qtyControls', {
        component: 'qtyControls',

        /**
         * Create function
         *
         * @private
         */
        create: function() {
           this.initControls();
        },

        /**
         * Init controls
         *
         * @private
         */
        initControls: function() {
            var self = this,
                el = $(self.element),
                updateBtn = $('.update-cart-item'),
                isMiniCart = el.closest('.minicart-items').length;

            el.find('.qty-control').on('click', function(e) {
                e.preventDefault();

                var iterator = $(this),
                    qtyInput = el.find('input.qty'),
                    currentQty = qtyInput.val(),
                    iteratorType = iterator.data('type');

                if (iteratorType === "decrement" && currentQty > 1) {
                    qtyInput.val(parseInt(currentQty) - 1);

                    if (isMiniCart) {
                        updateBtn.show();
                    }
                }

                if (iteratorType === "increment") {
                    qtyInput.val(parseInt(currentQty) + 1);

                    if (isMiniCart) {
                        updateBtn.show();
                    }
                }
            });
        }
    })
})();
