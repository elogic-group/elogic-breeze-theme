/**
 * Elogic Default - Footer accordion
 * Init on desktop, Destroy on mobile
 */

(function () {
    'use strict';

    $.widget('footerColumnAccordion', {
        component: 'footerColumnAccordion',
        options: {
            mediaBreakpoint: '(max-width: 767px)'
        },

        /**
         * Create function
         *
         * @private
         */
        create: function() {
            var self = this;

            /**
             * Init accordion on page load
             */
            this.accordionInit();

            /**
             * Init accordion on resize
             */
            $(document).on('breeze:resize', function (event, data) {
                self.accordionInit();
            });
        },

        /**
         * Init accordion on mobile
         * Destroy on desktop
         */
        accordionInit: function () {
            var opts = this.options,
                container = this.element,
                mql = window.matchMedia(opts.mediaBreakpoint),
                accordionOptions = {
                    header : '.footer-column__title',
                    content: '.footer-column__content',
                    animate: true
                }

            if (mql.matches) {
                container.each(function (index, elem) {
                    var element = $(elem),
                        accordion = element.collapsible(accordionOptions);

                    accordion.collapsible('close');
                    accordion.collapsible('enable');
                });
            } else {
                container.each(function (index, elem) {
                    var element = $(elem),
                        accordion = element.collapsible(accordionOptions);

                    accordion.collapsible('open');
                    accordion.collapsible('disable');
                });
            }
        }
    })
})();

