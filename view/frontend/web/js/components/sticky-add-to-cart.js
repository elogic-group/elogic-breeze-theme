/**
 * Elogic BreezeTheme - Sticky add to cart button
 * Copies add to cart box and makes it sticky on mobile
 */

(function () {
    'use strict';

    $.widget('stickyAddToCart', {
        component: 'stickyAddToCart',
        options: {
            pageProductConfigurable: 'page-product-configurable',
            formContainer: '.product-info-main',
            addToCart: '.product-add-form',
            configurableAddToCart: '.product-options-bottom',
            stickyAddToCart: '.sticky-addto',
            addToButton: '#product-addtocart-button',
            productAddToInput: '.product-info-main .box-tocart #qty',
            productAddToQty: '.product-info-main .box-tocart .qty-control',
            mediaBreakpoint: '(max-width: 767px)'
        },

        /**
         * Create function
         *
         * @private
         */
        _create: function() {
            var self = this,
                opts = this.options;

            var mql = window.matchMedia(opts.mediaBreakpoint);

            if (mql.matches) {
                self.cloneAddToCart();
                self.triggerAddToCart();
                self.showHideStickyBox();
            } else {
                $(opts.stickyAddToCart).remove();
            }
        },

        /**
         * Clone add to cart box
         */
        cloneAddToCart: function () {
            var opts = this.options,
                stickyAddToCart = $(opts.addToCart).clone();

            if ($('body').hasClass(opts.pageProductConfigurable)) {
                stickyAddToCart = $(opts.configurableAddToCart).clone();
            }

            $(opts.formContainer).append(stickyAddToCart);
            stickyAddToCart.addClass('sticky-addto');
        },

        /**
         * Check if add to cart block is visible on page scroll
         */
        isScrolledIntoView: function() {
            var opts = this.options,
                addToCart = $(opts.addToCart);

            if ($('body').hasClass(opts.pageProductConfigurable)) {
                addToCart = $(opts.configurableAddToCart);
            }

            var docViewTop = $(window).scrollTop(),
                docViewBottom = docViewTop + $(window).height(),
                elTop = addToCart.offset().top,
                elBottom = elTop + addToCart.height();

            return ((elBottom <= docViewBottom) && (elTop >= docViewTop));
        },

        /**
         * Show/Hide sticky add to cart box
         */
        showHideStickyBox: function () {
            var self = this,
                opts = this.options;

            window.addEventListener('scroll', function() {
                if (!self.isScrolledIntoView()) {
                    $(opts.stickyAddToCart).fadeIn(300);
                } else {
                    $(opts.stickyAddToCart).fadeOut(300);
                }
            });
        },

        /**
         * Trigger initial add to cart button
         * (ajax is not working on the second button)
         */
        triggerAddToCart: function () {
            var opts = this.options,
                button = $(opts.stickyAddToCart).find(opts.addToButton),
                stickyAddToInput =  $(opts.stickyAddToCart).find('input.qty'),
                productAddToInput = $(opts.productAddToInput),
                productAddToQty = $(opts.productAddToQty);

            productAddToQty.on('click', function (e) {
                stickyAddToInput.val($(productAddToInput).val());
            });

            this.triggerQtyButtons();

            button.on('click', function (e) {
                e.preventDefault();
                $(opts.addToButton).eq(0).trigger('click');
            });
        },

        /**
         * Trigger qty buttons in sticky add to cart
         */
        triggerQtyButtons: function () {
            var opts = this.options,
                productAddToInput = $(opts.productAddToInput);

            $(opts.stickyAddToCart).find('.qty-control').on('click', function(e) {
                e.preventDefault();

                var iterator = $(this),
                    qtyInput =  $(opts.stickyAddToCart).find('input.qty'),
                    currentQty = qtyInput.val(),
                    iteratorType = iterator.data('type');

                if (iteratorType === "decrement" && currentQty > 1) {
                    qtyInput.val(parseInt(currentQty) - 1);
                }

                if (iteratorType === "increment") {
                    qtyInput.val(parseInt(currentQty) + 1);
                }

                $(productAddToInput).val(qtyInput.val());
            });
        }
    })
})();
