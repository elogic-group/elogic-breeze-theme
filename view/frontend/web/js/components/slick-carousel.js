/**
 * Elogic BreezeTheme - Slick carousel
 * Carousel functionality
 */

(function () {
    'use strict';

    $.widget('carousel', {
        component: 'carousel',
        options: {
            arrows: false,
            dots: false,
            centerMode: false,
            infinite: false,
            progressBar: false,
            slidesCount: false
        },

        /** [create description] */
        create: function () {
            this.element.pagebuilderSlider(this.options);
        }
    });
})();
