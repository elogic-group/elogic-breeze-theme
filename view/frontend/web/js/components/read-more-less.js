/**
 * Elogic BreezeTheme - Read More/Less
 * Read More/Less functionality
 */

(function () {
    'use strict';

    $.widget('readMoreLess', {
        component: 'readMoreLess',

        /**
         * Default values
         */
        options: {
            targetElement: '.read-more',
            wordsCount: 80,
            toggle: true,
            moreLink: $.__('Read More'),
            lessLink: $.__('Read Less'),
            linkClass: 'action primary'
        },

        /**
         * Create function
         *
         * @private
         */
        create: function() {
            var opts = this.options;

            $readMoreJS({
                target: opts.targetElement,
                wordsCount: opts.wordsCount,
                toggle: opts.toggle,
                moreLink: opts.moreLink,
                lessLink: opts.lessLink,
                linkClass: opts.linkClass,
            });
        }
    });
})();
