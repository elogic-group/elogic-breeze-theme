/**
 * Magento Cart/Checkout - JS config
 */

var config = {
    map: {
        '*': {
            'qtyControls': 'Elogic_BreezeTheme/js/checkout-components/qty-controls',
            'showHidePass': 'Elogic_BreezeTheme/js/checkout-components/show-hide-pass',
            'carousel': 'Elogic_BreezeTheme/js/checkout-components/slick-carousel',
            'footerColumnAccordion': 'Elogic_BreezeTheme/js/checkout-components/footer'
        }
    },

    config: {
        mixins: {
            'mage/menu': {
                'Elogic_BreezeTheme/js/checkout-mixins/menu-mixin': true
            }
        }
    }
};
