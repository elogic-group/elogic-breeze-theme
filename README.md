# Elogic Breeze Theme module

> Configurations and common used JS functionality  


## Description

Extension is created to add some common used features missed in default Magento.

### List of the configurations added:
**Header**
1. Header Promotion
2. Enable Sticky Header
3. Enable Login Popup
4. Additional links

**Mobile Menu**
1. Enable Floating Header Links
2. Opening Hours

**Footer**
1. Footer columns
2. Column One Menu Links
3. Column Two Menu Links
4. Column Three Menu Links
5. Column Four Menu Links
6. Payment methods

**Social**
1.Social Media Links

**Checkout settings**
1. Minicart Type


### JS functionality

**Components**
1. footer.js
2. qty-controls.js
3. read-more-less.js
4. show-hide-pass.js
5. slick-carousel.js
6. sticky-add-to-cart.js

**Libraries**
1. read-more.min.js

**Mixins**
1. collapsible-mixin.js
2. menu-mixin.js
3. messages-mixin.js
4. slider-mixin.js


## Installation

1. Go to Magento2 root folder
2. Require this extension, enter following command to install extension: ```composer require elogic/module-breeze-theme```
3. Wait while composer is updated.

### OR

You can also download code from our repo:

***TODO: add link***

Enter following commands to enable the module:

```
php bin/magento module:enable Elogic_BreezeTheme
php bin/magento setup:upgrade
```


## Requirements

Magento installation


## Compatibility

Extension is created specifically for breeze-based theme

