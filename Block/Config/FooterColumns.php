<?php
namespace Elogic\BreezeTheme\Block\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Cms\Model\Wysiwyg\Config as WysiwygConfig;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use Elogic\BreezeTheme\Block\Config\ContentRenderer;

class FooterColumns extends AbstractFieldArray
{
    const TITLE = 'title';
    const CONTENT = 'content';

    /**
     * @var ContentRenderer
     */
    private $contentRenderer;

    public function __construct(
        ContentRenderer $contentRenderer,
        Context $context,
        array $data = [],
        ?SecureHtmlRenderer $secureRenderer = null
    ){
        $this->contentRenderer = $contentRenderer;
        parent::__construct($context, $data, $secureRenderer);
    }

    protected function _prepareToRender()
    {
        $this->addColumn(
            self::TITLE,
            [
                'label' => __('Title'),
                "class" => "required-entry"
            ]
        );
        $this->addColumn(
            self::CONTENT,
            [
                'label' => __('Content'),
                'renderer' => $this->getContentRenderer()
            ]
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Column');
    }

    public function getContentRenderer()
    {

        $this->contentRenderer = $this->getLayout()->createBlock(
            ContentRenderer::class,
            '',
            ['data' => ['is_render_to_js_template' => true]]
        );

        return $this->contentRenderer;
    }
}
